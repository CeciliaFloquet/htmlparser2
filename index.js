const axios = require('axios');
const htmlparser2 = require("htmlparser2");


let inTitle = false;
let countRows = false;
let done = false;
let rowCount = 0;

const parser = new htmlparser2.Parser(
    {
        onopentag(name, attribs) {
            if (name === "title") {
                inTitle = true;
            }
            if ( name === "tr" && countRows ) {
                rowCount++;
            }
        },
         onattribute(name, value) {
            if ( name === "class" && value === "table-responsive table-basic hide-mobile") {
                countRows = true;
            }
        },
        ontext(text) {
            if (inTitle) {
                console.log( "Page title: " + text );
            }
            inTitle = false;
        },
        onclosetag(tagname) {
            if (tagname === "html") {
                console.log("That's it!");
            }
            if ( tagname === "table" && countRows && !done ) {
                console.log( "There are " + (rowCount-1) + " computer labs.");
                done = true;
            }
        }
    },
    { decodeEntities: true }
);

if ( process.argv.length > 2 ) {
    axios.get( process.argv[2] )
        .then( response => {
            parser.write( response.data );
            parser.end();
        })
        .catch( error => {
            console.log("Could not fetch page.");
        });
} else {
    console.log( "Missing URL argument" );
}